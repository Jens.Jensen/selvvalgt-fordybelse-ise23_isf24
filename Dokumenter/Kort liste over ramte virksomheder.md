### Herunder er en liste over nogle af de virksomheder, der vil blive påvirket af NIS2:

1.	**Energi- og forsyningssektoren:** 
    - Virksomheder inden for produktion, distribution og forsyning af elektricitet, gas, vand og varme vil blive påvirket, da de udgør kritisk infrastruktur.

2.	**Transportsektoren:** 
    - Flyselskaber, jernbaneselskaber, rederier og andre transportvirksomheder, der driver vigtige infrastrukturer og tjenester, vil blive berørt af NIS2.

3.	**Finansielle institutioner:** 
    - Banker, forsikringsselskaber, betalingsudbydere og andre finansielle institutioner, der håndterer store mængder følsomme data og udfører transaktioner online, vil blive påvirket af NIS2.

4.	**Sundhedssektoren:** 
    - Hospitaler, lægepraksis, apoteker og andre sundhedsorganisationer, der behandler personlige og følsomme oplysninger om patienter og leverer vigtige sundhedstjenester, vil også være omfattet af NIS2.

5.	**Digitale serviceudbydere:** 
    - IT-serviceudbydere, cloud-tjenesteudbydere, sociale medieplatforme og andre digitale serviceudbydere, der leverer tjenester, der er afgørende for samfundet, vil være underlagt NIS2-reglerne.

6.	**Leverandører af digitale infrastrukturtjenester:** 
    - Virksomheder, der driver datacentre, internetudbydere, telekommunikationsselskaber og andre, der leverer den digitale infrastruktur, som andre virksomheder og samfundet er afhængige af, vil også blive berørt.