# Selvvalgt Fordybelse - Jens Schou Jensen

Dette gitlab projekt har til formål at samle og vise min dokumentation til det selvstændige læringsprojekt i faget Selvvalgt Fordybelse.

## Læringsmål
1. **At kunne få et øget læringsudbytte ved selvstændig læring, samt at kunne opstille en realistisk projektplan.**

2. **At opnå en dybere forståelse af NIS2-rammeværket og dets praktiske implikationer for danske virksomheder, herunder hvordan det påvirker cybersikkerhedspraksis og -krav.**

3. **At udvide mine analytiske færdigheder til at kunne evaluere en specifik virksomheds cybersikkerhedsforanstaltninger i overensstemmelse med NIS2-kravene og identificere områder til forbedring.**


![alt text](Billeder/Læringsmål.png)

Læringsmålene kan også findes under gitlab projektets "Issue boards"

## Projektmål
1. **At gennemføre en mere omfattende undersøgelse af NIS2-rammeværket, samt implementeringen inden for primært danske virksomheder for at opnå en dybdegående forståelse af dets relevans og konsekvenser.**

2. **At udvikle og en form for guide, primært rettet mod danske virksomheder, der skal kunne hjælpe dem med at få et hurtigt overblik over hvilke områder der skal lægges fokus på, for at kunne leve op til de kommende NIS2-krav.**

3. **At kunne præsentere mine resultater, opdagelser og refleksioner i en klar og sammenhængende rapport, der fokuserer på min selvstændige læringsproces, og samtidig bidrager til en øget forståelse af NIS2 og dets betydning.**


![alt text](Billeder/Projektmål.png)

Projektmålene kan også findes under gitlab projektets "Issue boards"

## Projektplan
![alt text](Billeder/Projektplan.png)

Projektplanen kan også findes under gitlab projektets "Issue boards"