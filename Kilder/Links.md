### Herunder er en liste med links til anvente kilder i forindelse med den selvstændige læringsproces om NIS2:

1. **[NIS2-direktivet](https://eur-lex.europa.eu/legal-content/DA/TXT/?uri=CELEX:32016L1148):** 
    - EU's officielle tekst om NIS2-direktivet, som beskriver dets formål, anvendelsesområde og krav til medlemsstaterne og virksomhederne.

2. **[PwC - NIS2 guide](https://www.pwc.dk/da/publikationer/cfo-cyberguide/nis2.html):**
    - PwC's guide til NIS2-direktivet, som giver en oversigt over direktivet, dets krav og konsekvenserne for danske virksomheder.

3. **[IT-Branchens NIS2-univers](https://itb.dk/nis2-univers/):**
    - IT-Branchens samling af information og vejledning om NIS2, herunder nyheder, vejledninger og cases fra danske virksomheder.

4. **[Dansk Standard - NIS2 Direktivet](https://www.ds.dk/da/om-standarder/viden/cyber-og-informationssikkerhedsstandarder/nis2-direktivet):**
    - Dansk Standards information om NIS2-direktivet, herunder baggrund, krav og vejledning til implementering.

5. **[PwC - Artikel om NIS2](https://www.pwc.dk/da/artikler/2022/05/nyt-cyberdirektiv-nis2-er-vedtaget.html):**
    - PwC's artikel om NIS2-direktivet, herunder en oversigt over kravene og konsekvenserne for danske virksomheder.

6. **[NIS2 artikel fra Version2](https://www.version2.dk/sponseret-indhold/nis2-compliance-stiller-nye-krav-til-cisoer-og-it-chefer):**
    - Artikel fra Version2 om NIS2-direktivet, herunder en gennemgang af kravene og udfordringerne for danske virksomheder.

7. **[NIS2-nyheder fra Computerworld](https://www.computerworld.dk/tag/nis2):**
    - Computerworld's samling af nyheder og artikler om NIS2, herunder cases, analyser og interviews med virksomheder og myndigheder.

8. **[NIS2-Direktivet (Net- og Informationssikkerhedsdirektivet)](https://www.ds.dk/da/i-fokus/lovgivning/lovgivning-paa-det-digitale-omraade-og-standarder/nis2-direktivet-net-og-informationssikkerhedsdirektivet):**
    - Dansk Standards information om den kommende NIS2 lovgivning.

9. **[Center for Cybersikkerhed - Status på NIS2](https://www.cfcs.dk/da/om-os/nis2/):**
    - Center for Cybersikkerheds information om NIS2, herunder status, krav og vejledning til virksomheder.

10. **[Dansk Industris NIS2 Vejledningsunivers](https://www.danskindustri.dk/vi-radgiver-dig/virksomhedsregler-og-varktojer/nis2-vejledningsunivers/):**
    - Dansk Industris samling af vejledninger og værktøjer om NIS2, herunder generel information, cases og analyser.